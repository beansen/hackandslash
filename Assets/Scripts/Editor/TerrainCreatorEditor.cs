﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TerrainCreator))]
public class TerrainCreatorEditor : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
        
		TerrainCreator myScript = (TerrainCreator)target;
		if(GUILayout.Button("Generate Heightmap"))
		{
			myScript.NewHeightMap();
		}
		if(GUILayout.Button("Generate Splatmap"))
		{
			myScript.GenerateSplatMap();
		}
		if(GUILayout.Button("Test"))
		{
			myScript.TestMethod();
		}
	}
}
