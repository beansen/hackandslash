﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class NpcController : MonoBehaviour
{

	public Transform npcSpawnPoints;
	public GameObject meleePrefab, rangedPrefab, projectilePrefab;

	private EventBus eventBus;

	private Dictionary<String, NpcBehavior> npcDict;
	private List<NpcBehavior> npcPool;
	
	private GameObject npcContainer;

	private int npcIndex = 1;
	
	private Transform player;

	private float npcDecisionTime;

	[Inject]
	public void Init(EventBus eventBus)
	{
		this.eventBus = eventBus;
		this.eventBus.Subscribe(EventBus.EventType.NpcProjectile, o =>
		{
			Pair<Vector3, Vector3> projectile = (Pair<Vector3, Vector3>) o;
		});
		this.eventBus.Subscribe(EventBus.EventType.NpcDamage, o =>
		{
			Pair<String, int> damage = (Pair<String, int>) o;
			if (npcDict.ContainsKey(damage.First))
			{
				NpcBehavior npc = npcDict[damage.First];
				npc.Damage(damage.Second);
				if (npc.Health <= 0)
				{
					npc.Die();
				}
			}
		});
	}

	// Update is called once per frame
	void Update () {
		if (npcDict != null)
		{
			npcDecisionTime += Time.deltaTime;
			if (npcDecisionTime >= 0.25f)
			{
				foreach (string key in npcDict.Keys)
				{
					npcDict[key].Update(player.position);
				}

				npcDecisionTime = 0;
			}
		}

		if (player)
		{
			for (int i = 0; i < npcSpawnPoints.childCount; i++)
			{
				if (Vector3.Distance(player.position, npcSpawnPoints.GetChild(i).position) < 25)
				{
					SpawnNpcs(npcSpawnPoints.GetChild(i).position);
					npcSpawnPoints.GetChild(i).parent = null;
					break;
				}
			}
		}
	}

	public void RegisterPlayer(Transform player)
	{
		this.player = player;
	}

	private void SpawnNpcs(Vector3 position)
	{
		if (npcContainer == null)
		{
			npcContainer = new GameObject("NPC");
		}

		if (npcDict == null)
		{
			npcDict = new Dictionary<string, NpcBehavior>();
			npcPool = new List<NpcBehavior>();
		}

		position.x += UnityEngine.Random.Range(-5f, 5f);
		position.z += UnityEngine.Random.Range(-5f, 5f);

		int amount = UnityEngine.Random.Range(5, 8);

		for (int i = 0; i < amount; i++)
		{
			Vector3 pos = position;
			pos.x += UnityEngine.Random.Range(-5f, 5f);
			pos.z += UnityEngine.Random.Range(-5f, 5f);
			
			if (npcPool.Count > 0)
			{
				/*NpcBehavior npcBehavior = npcPool[0];
				npcPool.RemoveAt(0);
				
				data.Reset();
				data.Go.transform.position = pos;
				npcDict.Add(data.Go.name, data);*/
			}
			else
			{
				String npcName = String.Format("NPC{0}", npcIndex++);

				if (i >= amount - 2)
				{
					GameObject go = Instantiate(rangedPrefab, npcContainer.transform, true);
					go.name = npcName;
					go.transform.position = pos;
					npcDict.Add(npcName, new RangedNpcBehavior(go, 10, eventBus));
				}
				else
				{
					GameObject go = Instantiate(meleePrefab, npcContainer.transform, true);
					go.name = npcName;
					go.transform.position = pos;
					npcDict.Add(npcName, new MeleeNpcBehavior(go, 10, eventBus));
				}
			}
		}
	}
}
