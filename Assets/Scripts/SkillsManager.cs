﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

public class SkillsManager : MonoBehaviour {

	public Transform effectTransform, handEffectTransform;
	public GameObject effectPrefab, handEffectPrefab;

	[Inject] private EventBus eventBus;

	private List<Effect> effectsPool;
	private List<Effect> activeEffects;

	private List<Effect> handEffectsPool;
	
	// Use this for initialization
	void Start () {
		effectsPool = new List<Effect>();
		activeEffects = new List<Effect>();
		handEffectsPool = new List<Effect>();
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = activeEffects.Count - 1; i >= 0; i--)
		{
			Effect effect = activeEffects[i];
			
			if (DateTime.UtcNow.Subtract(effect.ActivcationTime).TotalMilliseconds >= 2500)
			{
				activeEffects.RemoveAt(i);
				effect.GameObject.SetActive(false);
				if (effect.Type == Effect.EffectType.Hand)
				{
					handEffectsPool.Add(effect);
				}
				else
				{
					effectsPool.Add(effect);
				}
			}
		}
	}

	public void CastEffect(Vector3 target)
	{
		DateTime activationTime = DateTime.UtcNow;
		
		target.y = 0.2f;
		Effect effect = GetEffectFromPool();
		Vector3 startPos = effectTransform.position;
		startPos.y = 0.2f;
		effect.GameObject.transform.position =  startPos;
		effect.GameObject.transform.LookAt(target);
		effect.GameObject.SetActive(true);
		effect.ActivcationTime = activationTime;
		activeEffects.Add(effect);

		effect = GetHandEffectFromPool();
		effect.GameObject.transform.position = handEffectTransform.position;
		effect.GameObject.SetActive(true);
		effect.ActivcationTime = activationTime;
		activeEffects.Add(effect);
	}

	private Effect GetEffectFromPool()
	{
		if (effectsPool.Count == 0)
		{
			Effect effect = new Effect(Instantiate(effectPrefab), Effect.EffectType.Projectile);
			effect.GameObject.GetComponentInChildren<RFX4_TransformMotion>().CollisionEnter += (sender, info) =>
			{
				if (info.Hit.transform.tag.Equals("Monster"))
				{
					Pair<String, int> damage = new Pair<string, int>(info.Hit.transform.name, Random.Range(5, 11));
					eventBus.Post(EventBus.EventType.NpcDamage, damage);
				}

				effect.GameObject.SetActive(false);
				activeEffects.Remove(effect);
				effectsPool.Add(effect);
			};
			
			return effect;
		}
		else
		{
			Effect effect = effectsPool[0];
			effectsPool.RemoveAt(0);
			return effect;
		}
	}
	
	private Effect GetHandEffectFromPool()
	{
		if (handEffectsPool.Count == 0)
		{
			Effect effect = new Effect(Instantiate(handEffectPrefab), Effect.EffectType.Hand);
			//effect.GameObject.GetComponent<DeactivateAfterTime>().enabled = false;
			effect.GameObject.transform.SetParent(handEffectTransform);
			return effect;
		}
		else
		{
			Effect effect = handEffectsPool[0];
			handEffectsPool.RemoveAt(0);
			return effect;
		}
	}
}
