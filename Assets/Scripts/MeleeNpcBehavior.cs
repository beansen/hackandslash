using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class MeleeNpcBehavior : NpcBehavior
{
    private const float AttackRange = 2;
    
    public MeleeNpcBehavior(GameObject go, int health, EventBus eventBus) : base(go, health, eventBus)
    {
        npcType = NpcType.Melee;
        Range = 4f;
        AttackTime = 2500;
    }
    
    protected override void Action(Vector3 playerPosition)
    {
        if (CanSeePlayer(playerPosition))
        {
            float dist = Vector3.Distance(playerPosition, GameObject.transform.position);

            if (dist <= AttackRange)
            {
                
                if (DateTime.UtcNow.Subtract(LastAttackTime).TotalMilliseconds >= AttackTime)
                {
                    if (State != NpcState.Attacking)
                    {
                        State = NpcState.Attacking;
                        Agent.enabled = false;
                        Animator.SetBool(MovingAnimState, false);
                    }
                    GameObject.transform.LookAt(playerPosition);
                    Animator.SetBool(AttackAnimState, true);
                    EventBus.Post(EventBus.EventType.PlayerDamage, Random.Range(5, 11));
                    LastAttackTime = DateTime.UtcNow;
                }
            }
            else
            {
                if (!IsAttacking())
                {
                    if (State != NpcState.Walking)
                    {
                        State = NpcState.Walking;
                        Agent.enabled = true;
                        Animator.SetBool(MovingAnimState, true);
                    }
            
                    Agent.SetDestination(playerPosition);
                }
            }
        }
        else
        {
            if (State != NpcState.None)
            {
                State = NpcState.None;
                Agent.enabled = false;
                Animator.SetBool(MovingAnimState, false);
            }
        }
    }

    private bool CanSeePlayer(Vector3 playerPosition)
    {
        playerPosition.y = 0.5f;
        Vector3 npcPos = GameObject.transform.position;
        npcPos.y = 0.5f;

        Vector3 direction = (playerPosition - npcPos).normalized;
        RaycastHit hit;

        if (Physics.Raycast(npcPos, direction, out hit, 4))
        {
            return hit.transform.tag.Equals("Player");
        }

        return false;
    }
}