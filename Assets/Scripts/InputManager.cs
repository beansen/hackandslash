﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class InputManager : MonoBehaviour
{

	private InventoryManager inventoryManager;

	private PlayerController playerController;

	private bool mousePressed;

	private PlayerState currentPlayerState = PlayerState.None;
	
	// Update is called once per frame
	void Update () {
		if (playerController)
		{
			if (Input.GetKey(KeyCode.LeftShift))
			{
				if (Input.GetMouseButton(0))
				{
					Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;

					if (Physics.Raycast(ray, out hit, 50))
					{
						playerController.Attack(hit.point);
					}
				}
			}
			else
			{
				if (Input.GetMouseButton(0))
				{
					Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;

					if (Physics.Raycast(ray, out hit, 50))
					{
						if (mousePressed)
						{
							if (currentPlayerState == PlayerState.Moving)
							{
								playerController.Move(hit.point);
							}
							else
							{
								playerController.Attack(hit.point);
							}
						}
						else
						{
							if (hit.transform.tag.Equals("Monster"))
							{
								currentPlayerState = PlayerState.Attacking;
								playerController.Attack(hit.point);
							}
							else
							{
								currentPlayerState = PlayerState.Moving;
								playerController.Move(hit.point);
							}
						}
					}

					mousePressed = true;
				}
			}
		}
		
		if (Input.GetMouseButtonUp(0))
		{
			mousePressed = false;
		}
	}

	public void SetPlayerController(PlayerController playerController)
	{
		this.playerController = playerController;
	}
	
	private enum PlayerState
	{
		None,
		Attacking,
		Moving
	}
}


