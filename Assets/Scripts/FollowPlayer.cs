﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

	private Transform player;

	public Vector3 offset;
	public float distance = 5f;
	public float speed = 5f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (player != null)
		{
			Vector3 cameraPosition = player.position + offset;
			cameraPosition -= transform.forward * distance;

			transform.position = cameraPosition;
			//transform.position = Vector3.Lerp(transform.position, cameraPosition, speed * Time.deltaTime);
		}
	}

	public void SetPlayerTransform(Transform player)
	{
		this.player = player;
	}
}
