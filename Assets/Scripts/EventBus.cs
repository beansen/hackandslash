
using System;
using System.Collections.Generic;

public class EventBus
{
    private Dictionary<EventType, List<Action<Object>>> events;

    public EventBus()
    {
        events = new Dictionary<EventType, List<Action<Object>>>();
    }

    public void Subscribe(EventType eventType, Action<Object> action)
    {
        if (!events.ContainsKey(eventType))
        {
            events.Add(eventType, new List<Action<Object>>());
        }
        
        events[eventType].Add(action);
    }

    public void Post(EventType eventType, Object obj)
    {
        if (events.ContainsKey(eventType))
        {
            foreach (Action<object> action in events[eventType])
            {
                action(obj);
            }
        }
    }
    
    public enum EventType
    {
        PlayerDamage,
        NpcDamage,
        NpcProjectile
    }
}