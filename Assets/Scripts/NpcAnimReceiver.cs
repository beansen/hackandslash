﻿using System;
using UnityEngine;

public class NpcAnimReceiver : MonoBehaviour
{

	public Action AttackCallback;
	
	public void NpcAttack()
	{
		if (AttackCallback != null)
		{
			AttackCallback();
		}
	}
}
