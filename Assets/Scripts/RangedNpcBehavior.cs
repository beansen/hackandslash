
using System;
using UnityEngine;

public class RangedNpcBehavior : NpcBehavior
{
    public RangedNpcBehavior(GameObject go, int health, EventBus eventBus) : base(go, health, eventBus)
    {
        npcType = NpcType.Ranged;
        Range = 7;
        AttackTime = 3000;
    }
    
    protected override void Action(Vector3 playerPosition)
    {
        if (DateTime.UtcNow.Subtract(LastAttackTime).TotalMilliseconds >= AttackTime)
        {
            if (State != NpcState.Attacking)
            {
                State = NpcState.Attacking;
                Agent.enabled = false;
                Animator.SetBool(MovingAnimState, false);
            }
            GameObject.transform.LookAt(playerPosition);
            Animator.SetBool(AttackAnimState, true);
            LastAttackTime = DateTime.UtcNow;
        }
    }
}