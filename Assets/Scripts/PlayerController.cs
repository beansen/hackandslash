﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

public class PlayerController : MonoBehaviour
{

	private SkillsManager skillsManager;
	
	private Animator anim;

	private NavMeshAgent agent;

	private Vector3 target;

	// Use this for initialization
	void Start ()
	{
		anim = GetComponent<Animator>();
		agent = GetComponent<NavMeshAgent>();
		skillsManager = GetComponent<SkillsManager>();
	}
	
	[Inject]
	public void Init(InputManager inputManager, NpcController npcController)
	{
		inputManager.SetPlayerController(this);
		npcController.RegisterPlayer(transform);
	}
	
	// Update is called once per frame
	void Update () {
		if (anim)
		{
			anim.SetFloat("Speed", agent.velocity.magnitude);
		}
 	}

	public void ActivateEffect()
	{
		skillsManager.CastEffect(target);
	}

	public void Attack(Vector3 target)
	{
		if (agent.hasPath)
		{
			agent.ResetPath();
			anim.SetFloat("Speed", 0);
		}

		Quaternion rot = Quaternion.LookRotation((target - transform.position).normalized);
		rot.Set(0, rot.y, 0, rot.w);
		transform.rotation = rot;

		if (ReadyForInput())
		{
			this.target = target;
			anim.SetTrigger("Attack1");
		}
	}

	public void Move(Vector3 destination)
	{
		if (ReadyForInput())
			agent.SetDestination(destination);
	}

	private bool ReadyForInput()
	{
		return anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") || anim.GetCurrentAnimatorStateInfo(0).IsName("Running");
	}
}
