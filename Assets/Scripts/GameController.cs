﻿using UnityEngine;
using Zenject;

public class GameController : MonoBehaviour
{
	public GameObject playerPrefab;
	public Terrain terrain;

	private DiContainer container;
	private InputManager inputManager;

	[Inject]
	public void Init(NpcController npcController, DiContainer container)
	{
		this.container = container;
		SpawnPlayer();
	}

	private void SpawnPlayer()
	{
		TerrainData data = terrain.terrainData;
		
		int axis = Random.Range(0, 2);
		int x;
		int y;

		if (axis == 0)
		{
			x = Random.Range(0, 2) * (data.heightmapWidth - 1);
			y = data.heightmapHeight / 2;
			int direction = x == 0 ? 1 : -1;

			while (data.GetHeight(x, y) > 0)
			{
				x += direction;
			}

			x += direction * 2;
		}
		else
		{
			x = data.heightmapWidth / 2;
			y = Random.Range(0, 2) * (data.heightmapHeight - 1);
			int direction = y == 0 ? 1 : -1;

			while (data.GetHeight(x, y) > 0)
			{
				y += direction;
			}

			y += direction * 2;
		}
		
		GameObject go = container.InstantiatePrefab(playerPrefab);
		go.transform.position = new Vector3(x * (data.size.x / data.heightmapWidth), 0, y * (data.size.z / data.heightmapWidth));
		Camera.main.GetComponent<FollowPlayer>().SetPlayerTransform(go.transform);
	}
}
