﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class TerrainCreator : MonoBehaviour
{

    public int border = 20;

    public void GenerateHeightMap()
    {
        TerrainData data = GetComponent<Terrain>().terrainData;
        float[,] heights = new float[data.heightmapWidth, data.heightmapHeight];

       /* for (int y = 0; y < heights.GetLength(1); y++)
        {
            for (int x = 0; x < heights.GetLength(0); x++)
            {
                heights[x, y] = GetHeight(x, y, 10, 0.5f, 0.5f) * 1f - Mask(x, y, heights.GetLength(0));
            }
        }*/
        
        List<Vector2> points = new List<Vector2>();
        List<Vector2> newPoints = new List<Vector2>();
        
        points.Add(new Vector2(border, border));
        points.Add(new Vector2(heights.GetLength(0) - border, border));

        for (int j = 0; j < 10; j++)
        {
            for (int i = 0; i < points.Count - 1; i++)
            {
                Vector2 pointA = points[i];
                Vector2 pointB = points[i + 1];
                
                Vector2 mid = new Vector2((pointA.x + pointB.x) / 2, (pointA.y + pointB.y) / 2);
                Vector2 rot = Vector2.Perpendicular(pointB - pointA).normalized;

                int direction = Random.Range(0, 2) == 0 ? -1 : 1;
                mid += direction * rot * Random.Range(5f, 10f);
                
               newPoints.Add(mid);
            }

            for (int i = 0; i < newPoints.Count; i++)
            {
                points.Insert(i * 2 + 1, newPoints[i]);
            }
            
            newPoints.Clear();
        }

        for (int i = 0; i < points.Count; i++)
        {
            int x = (int) points[i].x;
            
            for (int y = (int) points[i].y; y >= 0; y--)
            {
                heights[x, y] = 1;
            }

            if (i < points.Count - 1)
            {
                List<Vector2> line = DdaAlgorithm(points[i], points[i + 1]);

                for (int j = 0; j < line.Count; j++)
                {
                    x = (int) line[j].x;
            
                    for (int y = (int) line[j].y; y >= 0; y--)
                    {
                        heights[x, y] = 1;
                    }
                }
            }
        }
        
        data.SetHeights(0, 0, heights);
    }

    public void GenerateSplatMap()
    {
        TerrainData data = GetComponent<Terrain>().terrainData;
        float[, ,] splatmapData = new float[data.alphamapWidth, data.alphamapHeight, data.alphamapLayers];
        
        for (int y = 0; y < splatmapData.GetLength(1); y++)
        {
            for (int x = 0; x < splatmapData.GetLength(0); x++)
            {
                float y_01 = y/(float)data.alphamapHeight;
                float x_01 = x/(float)data.alphamapWidth;
                float[] splatWeights = new float[data.alphamapLayers];
                float height = data.GetHeight(Mathf.RoundToInt(y_01 * data.heightmapHeight), Mathf.RoundToInt(x_01 * data.heightmapWidth));

                splatWeights[0] = Mathf.Clamp01(1f - height);
                splatWeights[1] = Mathf.Clamp01(height);
                float z = splatWeights.Sum();
                
                for (int i = 0; i < splatWeights.Length; i++)
                {
                    splatmapData[x, y, i] = splatWeights[i] / z;
                }
            }
        }
        
        data.SetAlphamaps(0, 0, splatmapData);
    }

    public void NewHeightMap()
    {
        int offset = Random.Range(0, 500);
        TerrainData data = GetComponent<Terrain>().terrainData;
        float[,] heights = new float[data.heightmapWidth, data.heightmapHeight];
        float scale = (300f / data.heightmapWidth) * 0.03f;

        int[] cutOff = new int[4];
        
        for (int i = 20; i <= heights.GetLength(0) - 20; i++)
        {
            float h = Mathf.PerlinNoise((i + offset) * scale, 0);
            int max = (int) (border * h);

            if (i == 20)
            {
                cutOff[0] = max - 4;
            }

            if (i == heights.GetLength(0) - 20)
            {
                cutOff[1] = max - 4;
            }
            
            for (int j = 0; j <= max; j++)
            {
                if (j <= max - 4)
                {
                    heights[i, j] = 1;
                }
                else
                {
                    heights[i, j] = 0.2f * (max - (j - 1)) + Random.Range(-0.01f, 0.01f);
                }
            }
            
            h = Mathf.PerlinNoise((i + offset) * scale, heights.GetLength(1) - 1);
            max = (int) (border * h);
            
            if (i == 20)
            {
                cutOff[2] = max - 4;
            }

            if (i == heights.GetLength(0) - 20)
            {
                cutOff[3] = max - 4;
            }
            
            for (int j = heights.GetLength(1) - 1; j >= heights.GetLength(1) - max; j--)
            {
                int step = heights.GetLength(1) - 1 - j;
                if (step <= max - 4)
                {
                    heights[i, j] = 1;
                }
                else
                {
                    heights[i, j] = 0.2f * (max - (step - 1)) + Random.Range(-0.01f, 0.01f);;
                }
            }
        }
        
        for (int i = 0; i < heights.GetLength(1); i++)
        {
            float h = Mathf.PerlinNoise(0, (i + offset) * scale);
            int max = (int) (border * h);

            if (i <= cutOff[0] || i >= heights.GetLength(1) - cutOff[2])
            {
                for (int j = 0; j < 20; j++)
                {
                    heights[j, i] = 1;
                }
            }
            else
            {
                for (int j = 0; j <= max; j++)
                {
                    if (j <= max - 4)
                    {
                        heights[j, i] = 1;
                    }
                    else
                    {
                        heights[j, i] = 0.2f * (max - (j - 1)) + Random.Range(-0.01f, 0.01f);;
                    }
                }
            }
            
            h = Mathf.PerlinNoise(heights.GetLength(0) - 1, (i + offset) * scale);
            max = (int) (border * h);

            if (i <= cutOff[1] || i >= heights.GetLength(1) - cutOff[3])
            {
                for (int j = heights.GetLength(0) - 1; j > heights.GetLength(0) - 20; j--)
                {
                    heights[j, i] = 1;
                }
            }
            else
            {
                for (int j = heights.GetLength(0) - 1; j >= heights.GetLength(0) - max; j--)
                {
                    int step = heights.GetLength(0) - 1 - j;
                    if (step <= max - 4)
                    {
                        heights[j, i] = 1;
                    }
                    else
                    {
                        heights[j, i] = 0.2f * (max - (step - 1)) + Random.Range(-0.01f, 0.01f);;
                    }
                }
            }
        }
        
        data.SetHeights(0, 0, heights);
    }
    
    private float GetHeight(int x, int y, int octaves, float persistency, float lacunarity, float scale)
    {
        float height = 0;
        float amplitude = 1f;
        float frequency = 1;
        float maxValue = 0;
        
        for (int i = 0; i < octaves; i++)
        {
            height += Mathf.PerlinNoise(x / scale * frequency, y / scale * frequency) * amplitude;
            maxValue += amplitude;
            amplitude *= persistency;
            frequency *= lacunarity;
        }
        
        return height/maxValue;
    }

    private float Mask(int x, int y, int size)
    {
        float distance_x = Mathf.Abs(x - size * 0.5f);
        float distance_y = Mathf.Abs(y - size * 0.5f);
        float distance = Mathf.Max(distance_x, distance_y);

        float max_width = size * 0.5f;
        float delta = distance / max_width;
        float gradient = delta * delta;

        return Mathf.Max(0.0f, 1.0f - gradient);
    }

    public void TestMethod()
    {
        Vector2 pointA = new Vector2(0, 15);
        Vector2 pointB = new Vector2(9, 0);

        List<Vector2> points = DdaAlgorithm(pointA, pointB);

        for (int i = 0; i < points.Count; i++)
        {
            Debug.Log(points[i]);
        }
    }

    private List<Vector2> DdaAlgorithm(Vector2 pointA, Vector2 pointB)
    {
        List<Vector2> points = new List<Vector2>();
        
        int dx = (int) (pointB.x - pointA.x);
        int dy = (int) (pointB.y - pointA.y);

        int steps = Mathf.Abs(dx) > Mathf.Abs(dy) ? Mathf.Abs(dx) : Mathf.Abs(dy);

        float xInc = dx / (float) steps;
        float yInc = dy / (float) steps;

        float x = pointA.x;
        float y = pointA.y;

        for (int i = 0; i < steps - 1; i++)
        {
            x += xInc;
            y += yInc;
            points.Add(new Vector2((int) x, (int) y));
        }

        return points;
    }
}
