
using System;
using UnityEngine;

public class Effect
{
    public GameObject GameObject;
    public DateTime ActivcationTime;
    public EffectType Type;

    public Effect(GameObject gameObject, EffectType type)
    {
        this.GameObject = gameObject;
        this.Type = type;
        this.GameObject.SetActive(false);
    }
    
    public enum EffectType
    {
        Projectile,
        Hand
    }
}