
using System;
using UnityEngine;
using UnityEngine.AI;

public abstract class NpcBehavior
{
    protected EventBus EventBus;
    
    protected DateTime LastAttackTime = DateTime.MinValue;
    protected float Range;
    protected uint AttackTime;
    protected Animator Animator;
    protected readonly GameObject GameObject;
    protected NpcState State = NpcState.None;
    protected readonly int AttackAnimState = Animator.StringToHash("Attack");
    protected readonly int MovingAnimState = Animator.StringToHash("Moving");
    protected readonly NavMeshAgent Agent;
    protected NpcType npcType;

    private int health;
    private DateTime deathTime = DateTime.MinValue;
    private readonly int deadAnimState = Animator.StringToHash("Dead");

    public int Health
    {
        get { return health; }
    }

    public NpcType Type
    {
        get { return npcType; }
    }

    public NpcBehavior(GameObject go, int health, EventBus eventBus)
    {
        this.GameObject = go;
        this.Animator = go.GetComponent<Animator>();
        this.Agent = go.GetComponent<NavMeshAgent>();
        this.health = health;
        this.EventBus = eventBus;
    }

    public void Damage(int damage)
    {
        health -= damage;
    }

    public void Die()
    {
        State = NpcState.Dead;
        deathTime = DateTime.UtcNow;
        Animator.SetBool(deadAnimState, true);
    }

    public void Reset()
    {
        health = 10;
        Agent.enabled = true;
        deathTime = DateTime.MinValue;
        GameObject.GetComponentInChildren<Collider>().enabled = true;
        GameObject.SetActive(true);
    }

    public void Update(Vector3 playerPosition)
    {
        if (State != NpcState.Dead)
        {
            if (IsInRange(playerPosition))
            {
                Action(playerPosition);
            }
            else
            {
                if (State != NpcState.Walking)
                {
                    State = NpcState.Walking;
                    Agent.enabled = true;
                    Animator.SetBool(MovingAnimState, true);
                }
            
                Agent.SetDestination(playerPosition);
            }
        }
    }
    
    private bool IsInRange(Vector3 playerPosition)
    {
        float dist = Vector3.Distance(GameObject.transform.position, playerPosition);
        return dist <= Range;
    }

    protected bool IsAttacking()
    {
        return Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack");
    }

    protected abstract void Action(Vector3 playerPosition);

    protected enum NpcState
    {
        None,
        Walking,
        Attacking,
        Dead
    }
    
    public enum NpcType
    {
        Ranged,
        Melee
    }
}
