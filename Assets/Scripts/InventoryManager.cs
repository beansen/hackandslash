﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class InventoryManager : MonoBehaviour
{

	public RectTransform itemsContainer;
	public GameObject itemPrefab;

	public List<Sprite> sprites;
	
	private int[,] inventorySpace;

	private const int width = 10;
	private const int height = 6;

	// Use this for initialization
	void Start () {
		inventorySpace = new int[width, height];
	}

	public void CreateItem()
	{
		float itemWidth = itemsContainer.rect.width / width;
		float itemHeight = itemsContainer.rect.height / height;
		
		Item item = new Item();
		item.width = Random.Range(1, 3);
		item.height = Random.Range(1, 4);
		AddItem(item);

		if (item.x != -1)
		{
			GameObject go = Instantiate(itemPrefab);
			go.transform.SetParent(itemsContainer);
			go.transform.localScale = Vector3.one;
			go.transform.GetChild(0).GetComponent<Image>().sprite = sprites[Random.Range(0, sprites.Count)];

			RectTransform rectTransform = go.GetComponent<RectTransform>();
			rectTransform.sizeDelta = new Vector2(itemWidth * item.width, itemHeight * item.height);
			rectTransform.anchoredPosition = new Vector2(rectTransform.sizeDelta.x / 2 + item.x * itemWidth, -(rectTransform.sizeDelta.y / 2 + item.y * itemHeight));
		}
		else
		{
			Debug.Log("Inventory full");
		}
	}

	private void AddItem(Item item)
	{
		bool itemAdded = false;
		
		for (int x = 0; x < width; x++)
		{
			for (int y = height - 1; y >= 0; y--)
			{
				if (inventorySpace[x, y] == 0)
				{
					if (x + (item.width - 1) <= width - 1 && y - (item.height - 1) >= 0)
					{
						bool spaceAvailable = true;

						for (int xPos = x; xPos < x + item.width; xPos++)
						{
							for (int yPos = y; yPos > y - item.height; yPos--)
							{
								if (inventorySpace[xPos, yPos] != 0)
								{
									spaceAvailable = false;
									break;
								}
							}

							if (!spaceAvailable)
							{
								break;
							}
						}

						if (spaceAvailable)
						{
							item.x = x;
							item.y = height - 1 - y;
							itemAdded = true;
							
							for (int xPos = x; xPos < x + item.width; xPos++)
							{
								for (int yPos = y; yPos > y - item.height; yPos--)
								{
									inventorySpace[xPos, yPos] = 1;
								}
							}
							
							break;
						}
					}
				}
			}
			
			if (itemAdded)
				break;
		}
	}
	
	public class Item
	{
		public int width;
		public int height;
		public int x = -1;
		public int y;
		public Sprite sprite;
	}
}
