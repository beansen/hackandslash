﻿using Zenject;

public class ZenInstaller : MonoInstaller {
	
	public override void InstallBindings()
	{
		Container.Bind<NpcController>().FromComponentInHierarchy().AsSingle();
		Container.Bind<InputManager>().FromComponentInHierarchy().AsSingle();
		Container.Bind<EventBus>().AsSingle().NonLazy();
	}
}
