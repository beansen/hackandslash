using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class MapDataGenerator
{
    private int width;
    private int height;

    private int walkableChance = 58;
    private int birthLimit = 3;
    private int blockLimit = 5;

    private CellData[,] cells;

    private Random rand;

    private int[] playerPosition;

    private int furthestPoint;

    private List<CellData> closeRange;
    private List<CellData> midRange;
    private List<CellData> longRange;

    public CellData[,] Cells
    {
        get { return cells; }
    }

    public MapDataGenerator(int width, int height)
    {
        this.width = width;
        this.height = height;
        
        cells = new CellData[width, height];
        rand = new Random();
        playerPosition = new int[2];
        
        closeRange = new List<CellData>();
        midRange = new List<CellData>();
        longRange = new List<CellData>();
    }

    public void Init()
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                cells[x, y] = new CellData();
                cells[x, y].Type = CellType.Normal;
                cells[x, y].X = x;
                cells[x, y].Y = y;
                cells[x, y].Walkable = rand.Next(0, 101) <= walkableChance;
                cells[x, y].Value = -1;
            }
        }
    }

    public void Step()
    {
        CellData[,] newCells = new CellData[width, height];
        
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int walkableNeighbours = WalkableNeighbours(x, y);
                newCells[x, y] = new CellData();
                newCells[x, y].Type = CellType.Normal;
                newCells[x, y].X = x;
                newCells[x, y].Y = y;
                newCells[x, y].Value = -1;
                
                if (cells[x, y].Walkable)
                {
                    if (walkableNeighbours < blockLimit)
                    {
                        newCells[x, y].Walkable = false;
                    }
                    else
                    {
                        newCells[x, y].Walkable = true;
                    }
                }
                else
                {
                    newCells[x, y].Walkable = walkableNeighbours > birthLimit;
                }
            }
        }

        cells = newCells;
    }

    public bool MapPlayable()
    {
        return furthestPoint > 30;
    }

    // TODO: Find better name
    public void CreateInitialData()
    {
        PlacePlayer();
        FloodFill();
    }

    public void PlaceObjects()
    {
        CloseUnreachableSections();
        PlaceEnemies();
        closeRange.Clear();
        midRange.Clear();
        longRange.Clear();
    }

    private void PlaceEnemies()
    {
        int steps = 0;
        List<CellData> found = new List<CellData>();
        int foundSize = 4;

        for (int i = 0; i < 3; i++)
        {
            List<CellData> list = null;

            switch (i)
            {
                case 0:
                    list = closeRange;
                    break;
                case 1:
                    list = midRange;
                    break;
                case 2:
                    list = longRange;
                    break;
            }

            while (found.Count < foundSize && list.Count > 0)
            {
                int index = rand.Next(0, list.Count);
                CellData cellData = list[index];

                if (found.Count == 0)
                {
                    if (HasSpaceToBorder(cellData.X, cellData.Y))
                    {
                        found.Add(cellData);
                        cellData.Type = CellType.Enemies;
                        list.RemoveAt(index);
                    }
                }
                else
                {
                    bool possible = true;
                    
                    for (int j = 0; j < found.Count; j++)
                    {
                        if (Vector2.Distance(cellData.Position, found[j].Position) < 10)
                        {
                            possible = false;
                            break;
                        }
                    }

                    if (possible && HasSpaceToBorder(cellData.X, cellData.Y))
                    {
                        found.Add(cellData);
                        cellData.Type = CellType.Enemies;
                        list.RemoveAt(index);
                    }
                }

                steps++;
                
                if (steps > 1000)
                    break;
            }
            
            found.Clear();
            foundSize++;
        }
    }

    private void PlacePlayer()
    {
        int axis = rand.Next(0, 2);
        int x = 0;
        int y = 0;

        if (axis == 0)
        {
            x = rand.Next(0, 2) * (width - 1);
            y = height / 2;
            int direction = x == 0 ? 1 : -1;

            while (!cells[x, y].Walkable)
            {
                x += direction;
            }

            x += direction * 2;
        }
        else
        {
            x = width / 2;
            y = rand.Next(0, 2) * (height - 1);
            int direction = y == 0 ? 1 : -1;

            while (!cells[x, y].Walkable)
            {
                y += direction;
            }

            y += direction * 2;
        }
        
        cells[x, y].Type = CellType.Player;
        cells[x, y].Value = 0;
        playerPosition[0] = x;
        playerPosition[1] = y;
    }

    private void FloodFill()
    {
        Queue<CellData> queue = new Queue<CellData>();
        
        queue.Enqueue(cells[playerPosition[0], playerPosition[1]]);

        while (queue.Count > 0)
        {
            CellData cellData = queue.Dequeue();

            for (int j = -1; j < 2; j++)
            {
                for (int i = -1; i < 2; i++)
                {
                    if (i != 0 || j != 0)
                    {
                        if (cellData.X + i >= 0 && cellData.X + i < width && cellData.Y + j >= 0 && cellData.Y + j < height)
                        {
                            CellData newCell = cells[cellData.X + i, cellData.Y + j];
                            
                            if (newCell.Walkable && newCell.Value == -1)
                            {
                                newCell.Value = cellData.Value + 1;
                                if (newCell.Value > furthestPoint)
                                    furthestPoint = newCell.Value;
                                queue.Enqueue(newCell);
                            }
                        }
                    }
                }
            }
        }
    }

    private void CloseUnreachableSections()
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (cells[x, y].Walkable)
                {
                    if (cells[x, y].Value == -1)
                        cells[x, y].Walkable = false;
                    
                    if (cells[x, y].Value >= furthestPoint * 0.2f - 4 && cells[x, y].Value <= furthestPoint * 0.2f + 4)
                        closeRange.Add(cells[x, y]);
                    
                    if (cells[x, y].Value >= furthestPoint * 0.5f && cells[x, y].Value <= furthestPoint * 0.5f + 5)
                        midRange.Add(cells[x, y]);
                    
                    if (cells[x, y].Value >= furthestPoint * 0.9f - 4 && cells[x, y].Value <= furthestPoint * 0.9f)
                        longRange.Add(cells[x, y]);
                }
            }
        }
    }
    
    private int WalkableNeighbours(int x, int y)
    {
        int alive = 0;

        for (int j = -1; j < 2; j++)
        {
            for (int i = -1; i < 2; i++)
            {
                if (i != 0 || j != 0)
                {
                    if (x + i >= 0 && x + i < width && y + j >= 0 && y + j < height)
                    {
                        if (cells[x + i, y + j].Walkable)
                        {
                            alive++;   
                        }
                    }
                }
            }
        }

        return alive;
    }

    private bool HasSpaceToBorder(int x, int y)
    {
        return x > 2 && x < width - 3 && y > 2 && y < height - 3;
    }
}

public class CellData
{
    public bool Walkable { set; get; }
    public int Value { set; get; }
    public CellType Type { set; get; }
    public int X { set; get; }
    public int Y { set; get; }
    public Vector2 Position
    {
        get { return new Vector2(X, Y);}
    }
}

public enum CellType
{
    Player,
    Treasure,
    Enemies,
    Normal
}